step to run:
- make sure has postgres DB on local
- create table as you wish ;)
- add table sequence with command:
```
CREATE SEQUENCE IF NOT EXISTS [your_desired_table_name]_id_seq
```
- my preference table is columns id(int8), name(varchar), whatsapp(varchar), longitude(decimal), latitude(decimal), address(varchar), created_at(timestamp), modified_at(timestamp), deleted_at(timestamp), and modified_by(int8), you can change it as much as you want ;) it is only my preference
- dont forget add .env file that contains several configurations, here the list
```
DB_HOST=[your database host]
DB_PORT=[your database port]
DB_USERNAME=[your database username]
DB_PASSWORD=[your database password]
DB_NAME=[your database name]
APP_PORT=[your app port]
TABLE_NAME=[your single table name, it is single table crud, so I post it here :p, if you have a plan to develop it into multi table, no need use this]
```
- run command ```npm install```
- run command ```node server.js```