var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');
const Pool = require('pg').Pool;
require('dotenv').config({path: __dirname + '/.env'})

const APP_PORT = process.env['APP_PORT'] || 3000;
const TABLE_NAME = process.env['TABLE_NAME'] || 'my_table';
 
// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
    type Entity {
        id: Int
        name: String
        whatsapp: String
        latitude: String
        longitude: String
        address: String
    }
    type Mutation {
        insert(name: String!, address: String!, whatsapp: String!, latitude: Int, longitude:Int): Entity
        update(id: Int!, name: String!, address: String!, whatsapp: String!, latitude: Int, longitude:Int): Entity
        delete(id: Int!): Boolean
    }
    type Query {
        get(id: Int!): Entity
        getList: [Entity]
        checkIfItWorks: String
    }
`);
 
// The root provides a resolver function for each API endpoint
var root = {
    checkIfItWorks: () => {
        return "Hello World!"
    },
    get: async (args) => {
        const client = await conn();
        const results = await client.query("SELECT * FROM public." + TABLE_NAME + " WHERE id=" + args.id + " AND deleted_at IS NULL");
        return results.rows[0];
    },
    delete: async (args) => {
        let id = args.id;

        const client = await conn();
        let results = await client.query("SELECT * FROM public." + TABLE_NAME + " WHERE id=" + args.id + " AND deleted_at IS NULL");
        
        if(results.rows.length > 0) {
            await client.query("UPDATE public." + TABLE_NAME + " SET deleted_at = NOW() WHERE id=" + id);
            return true;
        }
        return false;
    },
    update: async (args, req) => {
        let entity = {
            name: args.name,
            whatsapp: args.whatsapp,
            longitude: args.longitude,
            latitude: args.latitude,
            address: args.address
        }

        const client = await conn();
        let results = await client.query("SELECT * FROM public." + TABLE_NAME + " WHERE id=" + args.id);
        let entityDetail = results.rows[0];

        entityDetail.name = entity.name === null ? entityDetail.name : entity.name;
        entityDetail.whatsapp = entity.whatsapp === null ? entityDetail.whatsapp : entity.whatsapp;
        entityDetail.address = entity.address === null ? entityDetail.address : entity.address;
        entityDetail.latitude = entity.latitude === null ? 0 : entity.latitude;
        entityDetail.longitude = entity.longitude === null ? 0 : entity.longitude;

        let q = `UPDATE public.${TABLE_NAME} SET name = '${entityDetail.name}', whatsapp = '${entityDetail.whatsapp}', address = '${entityDetail.address}', latitude = ${entityDetail.latitude}, longitude = ${entityDetail.longitude}
                WHERE id = ${entityDetail.id}
        `;

        await client.query(q);

        return entityDetail;
    },
    getList: async () => {
        const client = await conn();
        const results = await client.query("SELECT * FROM public." + TABLE_NAME + " WHERE deleted_at IS NULL ORDER BY id ASC");

        return results.rows;
    },
    insert: async (args, req) => {
        const client = await conn();
        let entity = {
            name: args.name,
            whatsapp: args.whatsapp,
            longitude: args.longitude,
            latitude: args.latitude,
            address: args.address
        }

        const q = `INSERT INTO public.${TABLE_NAME} (id, name, address, whatsapp, latitude, longitude, created_at, modified_by)
                    VALUES(nextval('${TABLE_NAME}_id_seq'), '${entity.name}', '${entity.address}', '${entity.whatsapp}', ${entity.latitude}, ${entity.longitude}, NOW(), 0) RETURNING id`;
        
        let results = await client.query(q);

        let id = results.rows[0].id;
        entity.id = id;

        return entity;
    }
};
 
var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(APP_PORT);
console.log(`Running a GraphQL API server at http://localhost:${APP_PORT}/graphql`);

const DB_HOST = process.env['DB_HOST'] || 'localhost';
const DB_USER = process.env['DB_USERNAME'] || 'postgres';
const DB_NAME = process.env['DB_NAME'] || 'my_table';
const DB_PASSWORD = process.env['DB_PASSWORD'] || 'password-db';
const DB_PORT = process.env['DB_PORT'] || 5432; 

const conn = () => {
    const pool = new Pool({
        user: DB_USER,
        host: DB_HOST,
        database: DB_NAME,
        password: DB_PASSWORD,
        port: DB_PORT
    })

    return pool.connect();
}